import com.java.main.RandomNumber;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int minNumber = 0;
        int maxNumber = 0;
        boolean retry = true;

        System.out.println("Здравствуй путник!! Тебя пиветсвует приложение рандомайзер!! \n" +
                "************************************************************************ \n" +
                "Прежде чем мы начнём, я прошу теюя установить минимальную и максимальную границы от 1 до 500\n" +
                "Итак, чтобы установить минимальную границу - введи положительное целое число и нажми кнопку \"Enter\",\n" +
                "Затем, установит максимальную границу - введи положительное целое число и нажми кнопку  \"Enter\" once again");

        do {
            System.out.println("Введи позитивные целые числа не больше 500!");

            while (!scanner.hasNextInt()) {
                System.out.println("Это не целое положительно число! Попробуй ввести ещё раз!");
                scanner.next();
            }
            minNumber = scanner.nextInt();
            try {
                maxNumber = scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.next();
            }

        } while (minNumber <= 0 || maxNumber <= 0 || maxNumber > 500 || minNumber > 500);

        if (minNumber > maxNumber) {
            int tmp = maxNumber;
            maxNumber = minNumber;
            minNumber = tmp;
        }

        System.out.println("Минимальная граница: " + minNumber + " " + "и максимальная граница " + maxNumber);

        RandomNumber randomNumber = new RandomNumber(minNumber, maxNumber);
        while (retry) {
            System.out.println("Введи команды: generate, help, exit");
            String command = scanner.next();
            switch (command.toLowerCase()) {
                case "generate":{
                    System.out.println("Сгененрированное число:");
                    System.out.println(randomNumber.getGenerateNumber());
                    break;
                }
                case "help":{
                    info();
                    break;
                }
                case "exit":{
                    retry = false;
                    break;
                }
                default:{
                    System.out.println("Некорректный ввод!");
                    break;
                }

            }


        }

    }

    private static void info() {
        System.out.println("Ввеедите generate и нажмите \"Enter\"- для генерирования числа \n" +
                "Введите help и нажмите \"Enter\" - для вызова списка доступных команд \n"  +
                "Введите exit и нажмите \"Enter\" - для завершения программы");
    }
}


