package com.java.main;

import java.util.Arrays;
import java.util.Random;

public class RandomNumber {
    Random random = new Random();

    private int[] arrayRandomNumbers = new int[10];
    private int countNumbers = 0;
    private int min;
    private int max;

    public RandomNumber(int min, int max) {
        this.min = min;
        this.max = max;
    }

    private int getRandomNumber() {
        int randomNumber = random.nextInt(max - min) + min;
        return randomNumber;
    }

    public int getGenerateNumber() {
        if (arrayRandomNumbers.length - 1 >= countNumbers) {
            arrayRandomNumbers = Arrays.copyOf(arrayRandomNumbers, arrayRandomNumbers.length + 10);
        }
        int randomNumber = getRandomNumber();
            while (getCheck(randomNumber)) {
                randomNumber = getRandomNumber();
            }
            arrayRandomNumbers[countNumbers] = randomNumber;
            countNumbers++;
        return randomNumber;

    }

    private boolean getCheck(int randomNumber) {
        for (int i : arrayRandomNumbers) {
            if (randomNumber == i) {
                return true;
            }
        }
        return false;

    }
}
